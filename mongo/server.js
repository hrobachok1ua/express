const MongoClient = require("mongodb").MongoClient;
   
const Joi = require('joi')
const express = require('express')
const app = express()

app.use(express.json())

let db;

app.get('/', (req, res) => {
    res.send('<h1>Hi sdffrom express</h1>');
})

app.get('/api/users', (req, res) => {
    db.collection('users').find().toArray((err, docs)=>{
        if(err) {
            console.log(err)
            return res.sendStatus(500)
        }
        res.send(docs);
    })
})

app.get('/api/users/:id', (req, res) => {
    let user;
    db.collection('users').find().toArray((err, docs)=>{
        if(err) {
            console.log(err)
            return res.sendStatus(500)
        }
        //console.log(typeof docs)
        user = docs.find(e => {
            return ""+e['_id'] === req.params.id
        })
        if(!user) return res.status(404).send('Course wasn\'t found')
        else res.send(user);        
    })
})

app.post('/api/users', (req, res) => {
    const schema = {
        name: Joi.string().min(3).required()
    }

    const result = Joi.validate(req.body, schema)

    if(result.error) {
        return res.status(400).send(result.error.details[0].message)
    }
    
    const user = {
        name: req.body.name
    }
    db.collection('users').insert(user, function(err, result){
        if(err) {
            return res.sendStatus(500)
        }
    })
    res.send(user)
})

const port = process.env.port || 3000

const url = "mongodb://localhost:27017/";

const mongoClient = new MongoClient(url, { useNewUrlParser: true });
 
mongoClient.connect(function(err, client){
      
    db = client.db("usersdb");
    if(err){ 
        return console.log(err);
    }
    let user = {name: "Tomas", age: 23};
    app.listen(port, () => {
        db.collection("users").drop()
        db.collection("users").insertOne(user, function(err, result){
          
            if(err){ 
                return console.log(err);
            }
            console.log(result.ops);
        });
        console.log(`Listening onds port ${port}`);
    })
});
