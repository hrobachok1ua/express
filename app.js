const Joi = require('joi')
const express = require('express')
const app = express()

app.use(express.json())

const courses = [
    {key: 1, payload: {
        title: 'expressjs',
        description: 'nodejs',
        author: 'mongoDB'
    }},
    {key: 2, payload: {
        title: 'expressjs',
        description: 'nodejs',
        author: 'mongoDB'
    }}
]

app.get('/', (req, res) => {
    res.send('<h1>Hi sdffrom express</h1>');
})

app.get('/api/courses', (req, res) => {
    res.send(courses);
})

app.get('/api/courses/:id', (req, res) => {
    const course = courses.find(c => c.id === Number(req.params.id))
    if(!course) return res.status(404).send('Course wasn\'t found')
    else res.send(course)
})

app.post('/api/courses', (req, res) => {
    const schema = {
        name: Joi.string().min(3).required()
    }

    const result = Joi.validate(req.body, schema)

    if(result.error) {
        return res.status(400).send(result.error.details[0].message)
    }
    
    const course = {
        id: courses.length+1,
        name: req.body.name
    }
    courses.push(course)
    res.send(courses)
})

const port = process.env.port || 3000
app.listen(port, () => {
    console.log(`Listening onds port ${port}`);
})